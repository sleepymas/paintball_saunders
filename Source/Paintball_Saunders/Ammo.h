// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Paintball_SaundersCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Ammo.generated.h"

UCLASS()
class PAINTBALL_SAUNDERS_API AAmmo : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* mesh;

public:	
	// Sets default values for this actor's properties
	AAmmo();

	// Material access
	UPROPERTY(EditAnywhere)
		UMaterial* ammoMaterial;
	UPROPERTY(EditAnywhere)
		UMaterialInstanceDynamic* ammoMatInst;

	// Action on overlap
	UFUNCTION()
		void OnComponentBeginOverlap(class UPrimitiveComponent* OverlappedComp,
			class AActor* OtherActor,
			class UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);
	// Pointer to main character
	UPROPERTY(EditAnywhere)
		APaintball_SaundersCharacter* MyCharacter;
};
