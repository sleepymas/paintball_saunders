/*
	Access to terrain that can be modified when shot by the projectile
	We use many techniques here such as sub dividing and changing materials
*/

#include "Terrain.h"


// Constructor 
ATerrain::ATerrain()
{
	// Create components
	RootComponent = CreateAbstractDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	TerrainMesh = CreateAbstractDefaultSubobject<UProceduralMeshComponent>(TEXT("TerrainMesh"));
	TerrainMesh->AttachTo(RootComponent);

	indexA = 0;
	indexB = 1;
	indexC = 2;
}

// Create mesh on begin play
// This is really slow,
// Hangs start of the game for a solid 45 seconds
// Need to understand why this is so slow
void ATerrain::BeginPlay()
{
	Super::BeginPlay();
	GenerateMesh();
}

// This routine is where the slow down is most likely
// Create a mesh!
void ATerrain::GenerateMesh()
{
	// Create mesh section
	TerrainMesh->CreateMeshSection_LinearColor(0, verts, tris, normals, 
		UV0, vertColors, Tangents, true);

	// Set new verts to verts
	vertsNew = verts;

	// Recurse through tris and verts to create mesh
	if (recursion > 0)
	{
		for (int i = 0; i < recursion; i++)
		{
			for (int j = 0; j < tris.Num() / 3; j++)
			{
				subDivide(tris[indexA], tris[indexB], tris[indexC]);
			}
			verts.Empty();
			verts = vertsNew;

			tris.Empty();
			tris = trisNew;
			trisNew.Empty();

			indexA = 0;
			indexB = 1;
			indexC = 2;

			vertDict.Empty();
			indicesDict.Empty();
		}
		// Create sections now that we have info
		TerrainMesh->CreateMeshSection_LinearColor(0, verts, tris, normals,
			UV0, vertColors, Tangents, true);
	}
}

// sub divide aka break apart and make in to more tris
// so we can alter the mesh
void ATerrain::subDivide(int a, int b, int c)
{
	// Vectors for verts
	FVector va = verts[a];
	FVector vb = verts[b];
	FVector vc = verts[c];

	// Linearaly interpolate verts
	FVector vab = FMath::Lerp(va, vb, 0.5);
	FVector vbc = FMath::Lerp(vb, vc, 0.5);
	FVector vca = FMath::Lerp(vc, va, 0.5);

	// place holders
	iA = a;
	iB = b;
	iC = c;

	// set if we need to take action or not
	// Default to false
	bool vabDuplicate = false;
	bool vbcDuplicate = false;
	bool vcaDuplicate = false;

	// iterate through dict and check if duplicate
	for (int i = 0; i < vertDict.Num(); i++)
	{
		if (vab == vertDict[i])
		{
			vabDuplicate = true;
			iAB = indicesDict[i];
		}

		if (vbc == vertDict[i])
		{
			vbcDuplicate = true;
			iBC = indicesDict[i];
		}

		if (vca == vertDict[i])
		{
			vcaDuplicate = true;
			iCA = indicesDict[i];
		}
	}

	// If not duplicate store the value
	if (!vabDuplicate)
	{
		vertsNew.Add(vab);
		vertDict.Add(vab);
		indicesDict.Add(vertsNew.Num() - 1);
		iAB = vertsNew.Num() - 1;
	}
	if (!vbcDuplicate)
	{
		vertsNew.Add(vbc);
		vertDict.Add(vbc);
		indicesDict.Add(vertsNew.Num() - 1);
		iBC = vertsNew.Num() - 1;
	}
	if (!vcaDuplicate)
	{
		vertsNew.Add(vca);
		vertDict.Add(vca);
		indicesDict.Add(vertsNew.Num() - 1);
		iCA = vertsNew.Num() - 1;
	}

	// Finally make the new tris
	// from out subdivision
	trisNew.Add(iA);
	trisNew.Add(iAB);
	trisNew.Add(iCA);

	trisNew.Add(iCA);
	trisNew.Add(iBC);
	trisNew.Add(iC);

	trisNew.Add(iAB);
	trisNew.Add(iB);
	trisNew.Add(iBC);

	trisNew.Add(iAB);
	trisNew.Add(iBC);
	trisNew.Add(iCA);

	// Update index
	indexA = indexA + 3;
	indexB = indexB + 3;
	indexC = indexC + 3;
}

// Alter the terrain mesh
void ATerrain::AlterTerrain(FVector loc)
{
	// iterate over verts at location to modify them
	for (int i = 0; i < verts.Num(); i++)
	{
		// If we are in the loc radius then perform mesh update
		if (FVector(verts[i] - loc).Size() < radius)
		{
			verts[i] = verts[i] - depth;
			TerrainMesh->UpdateMeshSection(0, verts, normals,
				UV0, upVertColors, Tangents);
		}
	}
}
