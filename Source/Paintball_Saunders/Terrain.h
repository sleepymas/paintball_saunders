// Actor to modify terrain mesh live

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Terrain.generated.h"

UCLASS()
class PAINTBALL_SAUNDERS_API ATerrain : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATerrain();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Data for the terrain mesh so we can store and modify
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		UProceduralMeshComponent* TerrainMesh;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		TArray<FVector> verts;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		TArray<FVector> normals;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		TArray<FVector> vertsNew;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		TArray<int> tris;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		TArray<int> trisNew;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		TArray<FLinearColor> vertColors;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		int recursion;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		float radius;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Defaults")
		FVector depth;

	// Alters terrain
	UFUNCTION(BlueprintCallable)
		void AlterTerrain(FVector loc);

	// Arrays for mesh primitives
	TArray<FVector2D> UV0;
	TArray<FColor> upVertColors;
	TArray<FProcMeshTangent> Tangents;
	TArray<FVector> vertDict;
	TArray<int> indicesDict;

	// Place holder variables
	int indexA;
	int indexB;
	int indexC;
	int iA;
	int iB;
	int iC;
	int iAB;
	int iBC;
	int iCA;

	// Make the mesh and break it down
	void GenerateMesh();
	void subDivide(int a, int b, int c);
};
