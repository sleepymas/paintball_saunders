// Fill out your copyright notice in the Description page of Project Settings.

#include "Spawner.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/BoxComponent.h"
#include "Engine/World.h"


// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Add a box component so we can size the spawn area
	boxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	RootComponent = boxComp;

	// You can edit both of these in the UI so just default them for now
	// Set the extent
	boxComp->SetBoxExtent(boxExtent);
	// Set the number to spawn
	numSpawn = 10;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
	// Get location of actor as we will need it to spawn
	origin = ASpawner::GetActorLocation();
	//Do work, spawn the spawner actors
	spawnObject();
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Meat and potatoes, spawn some stuff
void ASpawner::spawnObject()
{
	// To make this more interesting instead of spawning
	// Just one object we spawn up to the number to spawn
	for (int i = 0; i < numSpawn; i++)
	{
		// Have fun with rotator and randomize the rot of each instance
		FRotator rot;
		rot.Pitch = FMath::FRandRange(-180.f, 180.f);
		rot.Yaw = FMath::FRandRange(-180.f, 180.f);
		rot.Roll = FMath::FRandRange(-180.f, 180.f);

		// Get location to spawn by using random area in bounds
		loc = UKismetMathLibrary::RandomPointInBoundingBox(origin, boxExtent);
		// Access to the world
		UWorld* world = GetWorld();

		// If we have access to the world spawn away
		if (world)
		{
			// Params to pass in when spawning
			FActorSpawnParameters spawnParams;
			// Actually spawn the actor now that we have all the info needed
			AActor* SpawnActorRef = world->SpawnActor<AActor>(actorToSpawn, loc, rot, spawnParams);
		}
	}
}


