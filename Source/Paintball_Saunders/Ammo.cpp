/*
	Ammo actor to raise or lower player ammo
*/

#include "Ammo.h"
#include "Runtime/Engine/Classes/Engine/World.h"

// Sets default values
AAmmo::AAmmo()
{
	// Build up static mesh to attach color material to
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	// Set as root component
	RootComponent = mesh;

	// Set up collision, we want overlap all
	mesh->SetCollisionEnabled(ECollisionEnabled::Type::QueryAndPhysics);
	mesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

	// Get mesh asset
	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT(
		"StaticMesh'/Game/Geometry/Meshes/ammoMesh.ammoMesh'"));
	mesh->SetStaticMesh(MeshAsset.Object);

	mesh->SetWorldScale3D(FVector(0.2, 0.2, 0.2));

	// Set up dynamic material for mesh
	static ConstructorHelpers::FObjectFinder<UMaterial>MaterialAsset(
		TEXT("Material'/Game/Materials/AmmoMat.AmmoMat'"));
	ammoMaterial = MaterialAsset.Object;
	ammoMatInst = UMaterialInstanceDynamic::Create(ammoMaterial, mesh);

	// Apply the material to the mesh
	mesh->SetMaterial(0, ammoMatInst);

	// Set color of ammo containers, red or blue
	int x = rand() % 10;
	if (x % 2 == 0)
	{
		ammoMatInst->SetVectorParameterValue("color", FLinearColor::Red);
	}
	else
	{
		ammoMatInst->SetVectorParameterValue("color", FLinearColor::Blue);
	}

	// bind overlap event in constructor
	mesh->OnComponentBeginOverlap.AddDynamic(this,
		&AAmmo::OnComponentBeginOverlap);
}

// Action on overlap
// If overlapping then cast to player
void AAmmo::OnComponentBeginOverlap(class UPrimitiveComponent* OverlappedComp,
	class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if ((OtherActor != nullptr) && (OtherActor != this))
	{
		FLinearColor color; // Storage container for clor

		// Only react to player so cast to player and check that it worked
		MyCharacter = Cast<APaintball_SaundersCharacter>(OtherActor);
		if (MyCharacter)
		{
			if (GEngine)
			{
				ammoMatInst->GetVectorParameterValue("color", color);
				if (color == FLinearColor::Blue)
				{
					//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Hit Ammo +"));
					MyCharacter->SetAmmo(true, 25); // Add ammo

				}
				if (color == FLinearColor::Red)
				{
					//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Hit Ammo -"));
					MyCharacter->SetAmmo(false, 15); // Remove ammo
				}
			}
			// Only able to pick up once so destroy
			Destroy();
		}
	}
}