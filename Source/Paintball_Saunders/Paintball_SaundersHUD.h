// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"
#include "Paintball_SaundersHUD.generated.h"

UCLASS()
class APaintball_SaundersHUD : public AHUD
{
	GENERATED_BODY()

public:
	APaintball_SaundersHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;
	// Action on game begin
	virtual void BeginPlay() override;
private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

	// Score widget
	UPROPERTY(EditAnywhere, Category = "Score")
		TSubclassOf<class UUserWidget> scoreWidgetClass;

	// Splash screen
	UPROPERTY(EditAnywhere, Category = "Splash")
		TSubclassOf<class UUserWidget> splashWidgetClass;

	//Widget current displayed
	UPROPERTY(EditAnywhere, Category = "Widget")
		class UUserWidget* currentWidget;

	// Change to gameplay hud
	UFUNCTION()
		void SetHud();

	// Timer and  bool for hud swap
	FTimerHandle DelayTime;
	bool waitOver;
};

