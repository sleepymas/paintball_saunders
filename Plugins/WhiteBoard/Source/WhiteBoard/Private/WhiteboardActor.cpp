/*
	This actor is used to create a white board that can be drawn on
	This works through render targets and materials
	As the player draws on the board the UV's are passed to render target
	And then the material shows through at those locations.
	There is a draw and clear routine at the heart of this
*/

#include "WhiteboardActor.h"
#include "Engine.h"
#include "Kismet/KismetRenderingLibrary.h"

// Sets default values
AWhiteboardActor::AWhiteboardActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	whiteboardMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT
	("Whiteboard Mesh"));
	RootComponent = whiteboardMesh;

	static ConstructorHelpers::FObjectFinder<UStaticMesh>mesh(TEXT(
		"StaticMesh'/WhiteBoard/WhiteBoardFiles/Plane.Plane'"));
	whiteboardMesh->SetStaticMesh(mesh.Object);

	static ConstructorHelpers::FObjectFinder<UMaterial>mat_mesh(TEXT(
		"Material'/WhiteBoard/WhiteBoardFiles/M_WhiteBoard.M_Whiteboard'"));
	whiteboardMesh->SetMaterial(0, mat_mesh.Object);

	static ConstructorHelpers::FObjectFinder<UMaterial>mat_marker(TEXT(
		"Material'/WhiteBoard/WhiteBoardFiles/M_Marker.M_Marker'"));
	markerMat = mat_marker.Object;

	static ConstructorHelpers::FObjectFinder<UTextureRenderTarget2D>rt(TEXT(
		"TextureRenderTarget2D'/WhiteBoard/WhiteBoardFiles/RT_WhiteBoard.RT_Whiteboard'"));
	renderTarget = rt.Object;
}

// Called when the game starts or when spawned
void AWhiteboardActor::BeginPlay()
{
	Super::BeginPlay();
	
	if (markerMat)
	{
		whiteboardMarker_DMI = UMaterialInstanceDynamic::Create(markerMat, this);
	}
	
	//Clear to black
	UKismetRenderingLibrary::ClearRenderTarget2D(this, renderTarget, FLinearColor::Black);
}

// Draw on the board by modifying materials
void AWhiteboardActor::drawOnWhiteboard(FVector2D loc, float drawSize)
{
	// Make sure we are above 0 for draw size
	if (drawSize < 0.01)
	{
		drawSize = 0.01;
	}
	// Set the draw size from input
	whiteboardMarker_DMI->SetScalarParameterValue("drawSize", drawSize);
	// draw at location
	whiteboardMarker_DMI->SetVectorParameterValue("drawLoc", FLinearColor(loc.X, loc.Y, 0));
	// Draw on the render target
	UKismetRenderingLibrary::DrawMaterialToRenderTarget(this, renderTarget, whiteboardMarker_DMI);
	drawnOn = true;
}

void AWhiteboardActor::clearWhiteboard()
{
	//Clear to black
	UKismetRenderingLibrary::ClearRenderTarget2D(this, renderTarget, FLinearColor::Black);
	drawnOn = false;
}
